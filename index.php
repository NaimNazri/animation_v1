<html>
    <head>
        <title>Third Test</title>
        <link rel="stylesheet" href="css/style.css">
    </head>   
    
    <body>
        <!-- Character -->
        <div id="character" >
            <img src="img/man_pose1.png" alt="pose1" id="first" 
                data-0="display:block;" 
                data-801="display:none;">

            <img src="img/man_pose2.png" alt="pose2" id="second" 
                data-800="opacity:0;"
                data-801="opacity:1;"  
                data-3200="display:block;" 
                data-3201="display:none;">
                
            <img src="img/man_pose3.png" alt="pose3" id="third" 
                data-3200="opacity:0;" 
                data-3201="opacity:1;" 
                data-4470="display:block;"
                data-4471="display:none;">
        </div>
        
        <div id="woot" 
            data-0="background-color:rgb(0,0,255);" 
            data-3200="background-color:rgb(255,0,0);"
        >WOOOT</div>
        
        <section id="base1">
            <img src="img/obj_type1.png" alt="questionMark1" class="q1" 
                    data-0="display:block;" 
                    data-300="opacity:1;"
                    data-301="opacity:0;">
            <img src="img/obj_type1.png" alt="questionMark2" class="q2" 
                    data-0="display:block;" 
                    data-300="opacity:1;"
                    data-301="opacity:0;">
            <img src="img/obj_type1.png" alt="questionMark3" class="q3" 
                    data-0="display:block;" 
                    data-300="opacity:1;"
                    data-301="opacity:0;">
        </section>
        <section id="base2">
            <img src="img/obj_direction.png" alt="direction" class="direction bounce-1" 
                    data-0="display:block;" 
                    data-800="opacity:0;"
                    data-801="opacity:1;"
                    data-1070="opacity:1;"
                    data-1071="opacity:0;">
            <img src="img/obj_type2.png" alt="type2" class="type2 bounce-1" 
                    data-0="display:block;" 
                    data-800="opacity:0;"
                    data-801="opacity:1;"
                    data-1070="opacity:1;"
                    data-1071="opacity:0;">

            
        </section>
        <section id="base3">
            <img src="img/obj_mathbook.png" alt="mathbook" class="mathbook bounce-1" 
                        data-0="display:block;" 
                        data-1600="opacity:0;"
                        data-1601="opacity:1;"
                        data-1890="opacity:1;"
                        data-1891="opacity:0;">
            <img src="img/obj_stethoscope.png" alt="Stethoscope" class="stethoscope bounce-1" 
                        data-0="display:block;" 
                        data-1600="opacity:0;"
                        data-1601="opacity:1;"
                        data-1890="opacity:1;"
                        data-1891="opacity:0;">
            <img src="img/obj_telescope.png" alt="Telescope" class="telescope bounce-1" 
                        data-0="display:block;" 
                        data-1600="opacity:0;"
                        data-1601="opacity:1;"
                        data-1890="opacity:1;"
                        data-1891="opacity:0;">
            <img src="img/obj_robotics.png" alt="Telescope" class="robotics bounce-1" 
                        data-0="display:block;" 
                        data-1600="opacity:0;"
                        data-1601="opacity:1;"
                        data-1890="opacity:1;"
                        data-1891="opacity:0;">
        </section>
        <section id="base4">
            <img src="img/obj_checklist.png" alt="checklist" class="checklist bounce-1" 
                        data-0="display:block;" 
                        data-2400="opacity:0;"
                        data-2401="opacity:1;"
                        data-2670="opacity:1;"
                        data-2671="opacity:0;">
        </section>
        <section id="base5">
            <img src="img/obj_flightbd.png" alt="flightbd" class="flightbd bounce-1" 
                        data-0="display:block;" 
                        data-3200="opacity:0;"
                        data-3201="opacity:1;"
                        data-3470="opacity:1;"
                        data-3471="opacity:0;">
            <img src="img/obj_flight.png" alt="flight" class="flight bounce-1" 
                        data-0="display:block;" 
                        data-3200="opacity:0;"
                        data-3201="opacity:1;"
                        data-3470="opacity:1;"
                        data-3471="opacity:0;">
            <img src="img/obj_luggage.png" alt="luggage" class="luggage bounce-1" 
                        data-0="display:block;" 
                        data-3200="opacity:0;"
                        data-3201="opacity:1;"
                        data-3470="opacity:1;"
                        data-3471="opacity:0;">
        </section>
        <section id="base6">
        
        </section>

        


        <script type="text/javascript" src="js/skrollr.js"></script>
        <script type="text/javascript">
        var s = skrollr.init();
        </script>
    </body>


</html>
